FROM alpine:3.12

LABEL description="This is an example Dockerfile for a minimal FreeCAD build in Alpine Linux"
LABEL warning="The image relies on VTK and OCC from the testing repo, which may not always be stable"
LABEL info="This FreeCAD build is missing smesh and workbenches depending on it, e.g. FEM"

ENV FREECAD_VER_TAG 0.19_pre
ENV PATCH_DIR alpine-freecad-0.19-patches

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing/" >> /etc/apk/repositories

ENV BUILD_DEP \
    git \
    make \
    cmake \
    gcc \
    g++ \
    patch \
    boost-dev \
    xerces-c-dev \
    zlib-dev \
    libexecinfo-dev \
    python3-dev \
    qt5-qtbase-dev \
    qt5-qtxmlpatterns-dev \
    eigen-dev \
    freeimage-dev \
    ffmpeg-dev \
    libxmu-dev \
    libxi-dev \
    vtk-dev \
    opencascade-dev

ENV RUNTIME_DEP \
    boost \
    xerces-c \
    zlib \
    libexecinfo \
    python3 \
    py3-six \
    qt5-qtbase \
    qt5-qtxmlpatterns \
    swig \
    eigen \
    freeimage \
    ffmpeg \
    libxmu \
    libxi \
    vtk \
    opencascade

RUN apk update && \
    apk add $RUNTIME_DEP && \
    apk add $BUILD_DEP --virtual .build_dep

WORKDIR /opt

RUN git clone https://github.com/FreeCAD/FreeCAD.git --branch $FREECAD_VER_TAG --depth 1

WORKDIR FreeCAD

COPY $PATCH_DIR $PATCH_DIR
RUN patch -p0 CMakeLists.txt < $PATCH_DIR/findBacktrace.patch && \
    patch -p0 src/App/CMakeLists.txt < $PATCH_DIR/linkBacktraceToApp.patch && \
    patch -p0 src/Main/CMakeLists.txt < $PATCH_DIR/linkBacktraceToMainCmd.patch && \
    patch -p0 cMake/FreeCAD_Helpers/SetupQt.cmake < $PATCH_DIR/findQt5Concurrent.patch

WORKDIR /opt/freecad-build

RUN cmake ../FreeCAD \
    -DCMAKE_BUILD_TYPE=Release \
    -DPYTHON_EXECUTABLE=/usr/bin/python3 \
    -DBUILD_QT5=ON \
    -DBUILD_GUI=OFF \
    -DFREECAD_USE_EXTERNAL_SMESH=ON \
    -DBUILD_FEM=OFF \
    -DBUILD_MESH_PART=OFF \
    -DBUILD_ARCH=OFF \
    -DBUILD_COMPLETE=OFF \
    -DBUILD_IMAGE=OFF \
    -DBUILD_INSPECTION=OFF \
    -DBUILD_MATERIAL=OFF \
    -DBUILD_MESH=OFF \
    -DBUILD_OPENSCAD=OFF \
    -DBUILD_PART_DESIGN=OFF \
    -DBUILD_PATH=OFF \
    -DBUILD_POINTS=OFF \
    -DBUILD_RAYTRACING=OFF \
    -DBUILD_REVERSEENGINEERING=OFF \
    -DBUILD_ROBOT=OFF \
    -DBUILD_SHOW=OFF \
    -DBUILD_START=OFF \
    -DBUILD_SURFACE=OFF \
    -DBUILD_TECHDRAW=OFF \
    -DBUILD_TUX=OFF \
    -DBUILD_WEB=OFF

RUN cmake --build . && cmake --install .

# create dummy Mesh module to enable loading some modules from Draft
RUN touch /usr/local/Mod/Draft/Mesh.py

RUN apk del .build_dep && \
    rm -rf /var/cache/apk/* && \
    rm -rf /opt/*

ENV PYTHONPATH /usr/local/lib64:$PYTHONPATH

WORKDIR /
