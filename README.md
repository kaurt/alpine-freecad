## Minimal FreeCAD build on Alpine Linux

This is an example Dockerfile for building FreeCAD on Alpine Linux. Most of the workbenches have been disabled and smesh is missing. Only the most basic modules like Part, Draft, Spreadsheet, etc. have been included. This is an off-screen build without any GUI modules, originally intended for use in Python applications. In order to make it compile on Alpine, some fixes were needed in CMake and have been provided as patch files.

Usage in Python

`# python3`

`>>> import FreeCAD`
